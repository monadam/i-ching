#+title: i ching

This is a website for the learning and practicing of the ancient Chinese I-Ching.

Live version is available at [https://i-ching.monadam.dev/].

* instructions

to build the website on your computer:

#+begin_src sh
git clone https://gitlab.com/monadam/i-ching
pnpm build
pnpm preview
#+end_src


* todo
** code
+ Link to hexagrams and trigrams within text.
+ Split hexagram name to subtitle
+ Optimize SEO

** design
+ Glyph display X and O for changing lines
+ Generate icon files
