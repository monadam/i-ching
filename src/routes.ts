import { RouteRecordRaw, createRouter, createWebHistory } from "vue-router";
import HomePage from "./pages/Home.vue";
import HexagramPage from "./pages/Hexagram.vue";
import BrowsePage from "./pages/Browse.vue";
import ReadingPage from "./pages/Reading.vue";
import TrigramPage from "./pages/Trigram.vue";
import AboutPage from "./pages/About.vue";

import { randomReadingValue } from "./model/reading";

const routes: RouteRecordRaw[] = [
  {
    path: "/",
    name: "home",
    component: HomePage,
  },
  {
    path: "/random",

    redirect: (to) => ({
      name: "reading",
      params: { value: randomReadingValue() },
    }),
  },
  {
    name: "reading",
    path: "/reading/:value",
    component: ReadingPage,
    props: true,
  },
  {
    path: "/about",
    component: AboutPage,
  },
  {
    path: "/browse",
    component: BrowsePage,
  },
  {
    name: "hexagram",
    path: "/hexagram/:value",
    component: HexagramPage,
  },
  {
    path: "/trigram/:value",
    component: TrigramPage,
  },
];

export default routes;
