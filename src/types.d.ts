declare interface TrigramData {
  id: number;
  value: string;
  name: string;
  cename: string;
  element: string;
  role: "parent" | "eldest" | "middle" | "youngest";
  gender: "male" | "female";
  // direction: "north" | "south" | "east" | "west";
}

declare interface HexagramData {
  id: number;
  value: string;
  name: string;
  cname: string;
  cename: string;
  commentary: string;
  image: string;
  imageCommentary: string;
  judgement: string;
  judgementCommentary: string;
  lines: string[];
  linesCommentary: string[];
}
