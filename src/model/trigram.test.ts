import { expect, test } from "vitest";
import { Trigram, trigrams } from "./trigram";

const creative = Trigram("777");
const peace = Trigram("888");

test("returns a list of trigrams", () => {
  expect(trigrams.length).toBe(8);
});

test("trigram has top and bottom trigrams", () => {
  console.log(creative.toString());
});
