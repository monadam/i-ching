import { expect, test } from "vitest";
import { Hexagram, hexagrams } from "./hexagram";

const creative = Hexagram("977777");
const peace = Hexagram("777886");

console.log(creative.toString());
console.log();
console.log(peace.toString());

test("returns a list of hexagrams", () => {
  expect(hexagrams.length).toBe(64);
});

test("hexagram has top and bottom trigrams", () => {
  expect(creative.bottom.value).toBe("977");
  expect(creative.top.value).toBe("777");
  expect(peace.bottom.value).toBe("777");
  expect(peace.top.value).toBe("886");
  expect(peace.top.name).toBe("The Receptive");
});
