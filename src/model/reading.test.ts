import { expect, test } from "vitest";
import { randomReadingValue, RandomReading, Reading } from "./reading";

const reading = Reading("999699");

const random = RandomReading();

test("creates random value", () => {
  expect(randomReadingValue()).toBeDefined();
});

test("creates random", () => {
  expect(random.hexagram).toBeDefined();
});

test("reading has present and future", () => {
  expect(reading.present.value).toBe("777877");
  expect(reading.future.value).toBe("888788");
});
