import * as Data from "../data";
import { presentLine, futureLine } from "./line";

export function Trigram(value: string) {
  const presentValue = value.split("").map(presentLine).join("");
  const futureValue = value.split("").map(futureLine).join("");

  const data = Data.trigrams.find((trigram) => trigram.value === presentValue);

  return {
    ...data,
    value,
    glyph: String.fromCodePoint(0x262f + data.id),
    cname: String.fromCodePoint(0x9775 + data.id),
    toString() {
      const lines = value.split("").map(
        (v) =>
          ({
            "6": "=== o ===",
            "7": "=========",
            "8": "===   ===",
            "9": "====X====",
          })[v],
      );
      lines[1] += " " + data.element;

      return lines.reverse().join("\n");
    },
  };
}

export const trigrams = Data.trigrams.map((data) => Trigram(data.value));
