import * as Data from "../data";

import { Trigram } from "./trigram";

import { presentLine, futureLine } from "./line";

export function Hexagram(val: string) {
  const value = val.split("").reverse().join("");
  const presentValue = value.split("").map(presentLine).join("");

  const data = Data.hexagrams.find(
    (hexagram) => hexagram.value === presentValue,
  );

  const bottom = Trigram(value.slice(0, 3));
  const top = Trigram(value.slice(3, 6));

  return {
    ...data,
    top,
    bottom,
    value,
    glyph: String.fromCodePoint(0x4dbf + data.id),
    toString: () => {
      const name = data.name;
      const parts = [bottom, top]
        .map((t) => t.toString())
        .reverse()
        .join("\n");
      return [data.name, parts].join("\n");
    },
  };
}

export const hexagrams = Data.hexagrams.map(({ value }) => Hexagram(value));
