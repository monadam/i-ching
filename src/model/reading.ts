import { presentLine, futureLine } from "./line";
import { Hexagram } from "./hexagram";

export function randomReadingValue(): string {
  const coin = () => (Math.random() < 0.5 ? 2 : 3);
  const line = () => {
    const [one, two, three] = [coin(), coin(), coin()];
    if (two + three === 4) {
      if (one === 2) return 2 + 2 * coin();
      if (one === 3) return 13 - 2 * coin();
    }
    return one + two + three;
  };
  const lines = new Array(6).fill(0).map(() => line());
  return lines.join("");
}

export function RandomReading() {
  return Reading(randomReadingValue());
}

export function Reading(value: string) {
  const presentValue = value.split("").map(presentLine).join("");
  const futureValue = value.split("").map(futureLine).join("");
  return {
    value,
    hexagram: Hexagram(value),
    present: Hexagram(presentValue),
    future: Hexagram(futureValue),
  };
}
