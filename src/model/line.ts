export function presentLine(value) {
  return {
    "6": "8",
    "7": "7",
    "8": "8",
    "9": "7",
  }[value];
}

export function futureLine(value) {
  return {
    "6": "7",
    "7": "7",
    "8": "8",
    "9": "8",
  }[value];
}
