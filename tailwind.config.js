/** @type {import('tailwindcss').Config} */
export default {
  content: ["./index.html", "./src/**/*.{vue,js,ts,jsx,tsx}"],
  separator: "_",
  theme: {
    extend: {
      fontFamily: { title: ['"Comfortaa"'] },
    },
  },
  plugins: [],
};
